from calibration import calibration

import numpy as np
from matplotlib import pyplot as plt
import cv2
from glob import glob
import random

# SETTINGS
RUN_DOCUMENT_RESULTS = False
OPENCV_CHECK = False
RANSAC_ITER_NUM = 10000
FOLDER_NAME = './stereo_image/*.png'

# global variables
K_matrix = np.array([[462.07997291, 0.0, 322.28293931], [0.0, 461.4164894, 180.27669611], [0.0, 0.0, 1.0]])
key_point1 = None
key_point2 = None
match_list = None
inlier_points = None
img_width = 0
img_height = 0
fundamental_matrix = None
if OPENCV_CHECK:
    fundamental_matrix_opencv = None

def task_1_1_intrinsic_calibration():
    print('task 1-1: camera intrinsic calibration')
    print('checker board images in \'images/\' are used to calibrate intrinsic parameter of camera')
    print('lense distortion is corrected and the results are in \'corrected_images/\'')
    calibration()


def task_1_2_snapshots_for_stereo_matching():
    print('task 1-2: snapshots for stereo matching')
    print('2 images are used in \'stereo_image\' folder')

    # load images
    img_names = glob(FOLDER_NAME)
    img1 = cv2.imread(img_names[0])
    img2 = cv2.imread(img_names[1])

    # build montage to show
    np_horizontal = np.hstack((img1, img2))
    cv2.imshow('task_1_2', np_horizontal)


def task_1_3_feature_matching():
    print('task 1-3: feature matching')
    print('find corresponding features between two images in \'stereo_image\' folder')

    global key_point1, key_point2, match_list, img_width, img_height

    # load image
    img_names = glob(FOLDER_NAME)
    img1 = cv2.imread(img_names[0], 0)
    img2 = cv2.imread(img_names[1], 0)
    # img1 = cv2.imread('./data/aloeL.jpg', 0)
    # img2 = cv2.imread('./data/aloeR.jpg', 0)

    img_width = img1.shape[1]
    img_height = img1.shape[0]

    # Initiate ORB detector
    orb = cv2.ORB_create()

    # find the key_points and descriptors
    kp1, des1 = orb.detectAndCompute(img1, None)
    kp2, des2 = orb.detectAndCompute(img2, None)

    key_point1 = kp1
    key_point2 = kp2

    # create BFMatcher object
    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

    # Match descriptors.
    matches = bf.match(des1, des2)

    # Sort them in the order of their distance.
    matches_sorted = sorted(matches, key=lambda x: x.distance)
    match_list = matches

    # show result
    res = cv2.drawMatches(img1, kp1, img2, kp2, matches_sorted, None)
    cv2.imshow('task_1_3', res)


def task_1_4_compute_fundamental_matrix():
    global key_point1, key_point2, match_list, fundamental_matrix, inlier_points
    if OPENCV_CHECK:
        global fundamental_matrix_opencv

    key_point_set = []
    for match in match_list:
        key_point_set.append([key_point1[match.queryIdx].pt, key_point2[match.trainIdx].pt])
    np_key_point_set = np.array(key_point_set)
    print(np_key_point_set.shape)

    def pickRandom8PtPairs():
        sample_idx = random.sample(range(np_key_point_set.shape[0]), 8)
        # print(sample_idx)
        x1 = np_key_point_set[sample_idx, 0, 0]
        x2 = np_key_point_set[sample_idx, 1, 0]
        y1 = np_key_point_set[sample_idx, 0, 1]
        y2 = np_key_point_set[sample_idx, 1, 1]
        return x1, x2, y1, y2

    def calcFundamentalMatrix(x1, x2, y1, y2):
        # normalize coordinate
        normalize_matrix = np.array([[2.0/img_width, 0, -1], [0, 2.0/img_height, -1], [0, 0, 1]])
        normalized_x1 = normalize_matrix.dot(np.array([x1, y1, 1]))
        normalized_x2 = normalize_matrix.dot(np.array([x2, y2, 1]))
        x1 = normalized_x1[0]
        y1 = normalized_x1[1]
        x2 = normalized_x2[0]
        y2 = normalized_x2[1]
        # x1, x2, y1, y2 = normalizePoint(x1, x2, y1, y2)
        # print(x1, x2, y1, y2)

        mInput = np.ones((9, 9))
        for pt_num in range(8):
            mInput[pt_num, 0] = x2[pt_num] * x1[pt_num]
            mInput[pt_num, 1] = x2[pt_num] * y1[pt_num]
            mInput[pt_num, 2] = x2[pt_num]
            mInput[pt_num, 3] = y2[pt_num] * x1[pt_num]
            mInput[pt_num, 4] = y2[pt_num] * y1[pt_num]
            mInput[pt_num, 5] = y2[pt_num]
            mInput[pt_num, 6] = x1[pt_num]
            mInput[pt_num, 7] = y1[pt_num]

        # print(mInput)
        u, s, vh = np.linalg.svd(mInput, full_matrices=True, compute_uv=True)
        # print(vh[:,-1])
        # print(np.matmul(mInput, vh[-1, :].transpose()))
        # print(s)
        v_fundamental = vh[-1, :]
        m_fundamental = np.reshape(v_fundamental, (3, 3))
        # print(vOutput, mOutput)

        # singularity constraint
        u, s, vh = np.linalg.svd(m_fundamental, full_matrices=True, compute_uv=True)
        m_singular = np.diag(s)
        # print(m_singular)
        m_singular[2, 2] = 0.0
        # print(m_singular)
        m_fundamental = np.dot(u, np.dot(m_singular, vh))
        res = np.matmul(normalize_matrix.transpose(), np.matmul(m_fundamental, normalize_matrix))
        # print(res)
        return res

    def calcEpipolarError(pt1, pt2, m_fundamental):
        # x_pt1, x_pt2, y_pt1, y_pt2 = normalizePoint(pt1[0], pt2[0], pt1[1], pt2[1])
        x_pt1 = pt1[0]
        x_pt2 = pt2[0]
        y_pt1 = pt1[1]
        y_pt2 = pt2[1]

        x1 = np.array([x_pt1, y_pt1, 1])
        x2 = np.array([x_pt2, y_pt2, 1])
        # print(x1, x2, m_fundamental)
        # error = np.power(np.matmul(np.matmul(x2, m_fundamental), x1.transpose()), 2)
        line = m_fundamental.dot(x1)
        error = np.abs(line.dot(x2))/np.sqrt(line[0]*line[0]+line[1]*line[1])
        # print(error)
        return error

    max_num_concensus = 0
    fundamental_matrix = None
    for i in range(RANSAC_ITER_NUM):
    # for i in range(1):
        x_query, x_train, y_query, y_train = pickRandom8PtPairs()
        f_matrix = calcFundamentalMatrix(x_query, x_train, y_query, y_train)
        num_concensus = 0
        concensus_set = []
        for j in range(np_key_point_set.shape[0]):
            pt_query = np_key_point_set[j, 0, :]
            pt_train = np_key_point_set[j, 1, :]
            if calcEpipolarError(pt_query, pt_train, f_matrix) < 1:
                num_concensus = num_concensus + 1
                concensus_set.append([pt_query, pt_train])
            # error_list.append(calcEpipolarError(pt_query, pt_train, f_matrix))

        # print(num_concensus, max_num_concensus)
        if num_concensus > max_num_concensus:
            max_num_concensus = num_concensus
            fundamental_matrix = f_matrix
            inlier_points = concensus_set
    print(max_num_concensus)
    print(f_matrix)

    if (OPENCV_CHECK):
        # opencv codes
        print('opencv res')
        good = []
        pts1 = []
        pts2 = []

        pts1 = np.int32(np_key_point_set[:, 0, :])
        pts2 = np.int32(np_key_point_set[:, 1, :])
        F, mask = cv2.findFundamentalMat(pts1, pts2, cv2.FM_RANSAC)
        # F, mask = cv2.findFundamentalMat(pts1, pts2, cv2.FM_8POINT)
        print(np.sum(mask))
        pts1 = pts1[mask.ravel() == 1]
        pts2 = pts2[mask.ravel() == 1]

        def drawlines(img1, img2, lines, pts1, pts2):
            ''' img1 - image on which we draw the epilines for the points in img2
                lines - corresponding epilines '''
            r, c = img1.shape
            img1 = cv2.cvtColor(img1, cv2.COLOR_GRAY2BGR)
            img2 = cv2.cvtColor(img2, cv2.COLOR_GRAY2BGR)
            for r, pt1, pt2 in zip(lines, pts1, pts2):
                color = tuple(np.random.randint(0, 255, 3).tolist())
                x0, y0 = map(int, [0, -r[2] / r[1]])
                x1, y1 = map(int, [c, -(r[2] + r[0] * c) / r[1]])
                img1 = cv2.line(img1, (x0, y0), (x1, y1), color, 1)
                img1 = cv2.circle(img1, tuple(pt1), 5, color, -1)
                img2 = cv2.circle(img2, tuple(pt2), 5, color, -1)
            return img1, img2

        # Find epilines corresponding to points in right image (second image) and
        # drawing its lines on left image
        # print(pts2.reshape(-1, 1, 2))
        lines1 = cv2.computeCorrespondEpilines(pts2.reshape(-1, 1, 2), 2, F)
        lines1 = lines1.reshape(-1, 3)

        img_names = glob(FOLDER_NAME)
        img1 = cv2.imread(img_names[0], 0)
        img2 = cv2.imread(img_names[1], 0)

        img5, img6 = drawlines(img1, img2, lines1, pts1, pts2)
        # Find epilines corresponding to points in left image (first image) and
        # drawing its lines on right image
        # print(pts1.reshape(-1, 1, 2))
        lines2 = cv2.computeCorrespondEpilines(pts1.reshape(-1, 1, 2), 1, F)
        lines2 = lines2.reshape(-1, 3)
        img3, img4 = drawlines(img2, img1, lines2, pts2, pts1)
        np_horizontal = np.hstack((img5, img3))
        cv2.imshow('opencv', np_horizontal)
        print(F)
        fundamental_matrix_opencv = F


def task_1_4_optional_draw_epipolar_line():
    pts1 = []
    pts2 = []
    for pts in inlier_points:
        pts1.append(pts[0])
        pts2.append(pts[1])

    def normalizePoint(point):
        x1 = (point[0] - img_width / 2.0) / (img_width / 2.0)
        y1 = (point[1] - img_height / 2.0) / (img_height / 2.0)
        return np.array([x1, y1])

    def reconstructPoint(normalized_line):
        a = normalized_line[0] / (img_width / 2.0)
        b = normalized_line[1] / (img_height / 2.0)
        c = - normalized_line[0] - normalized_line[1] + normalized_line[2]
        return a, b, c

    def sceneTransform(point):
        # point = normalizePoint(point)
        # transformed = np.matmul(fundamental_matrix, np.array([point[0], point[1], 1.0]).transpose())
        transformed = fundamental_matrix.dot(np.array([point[0], point[1], 1.0]))
        return transformed
        # return reconstructPoint(transformed)
        # transformed = np.matmul(fundamental_matrix_opencv, np.array([point[0], point[1], 1.0]))
        # return transformed[0], transformed[1], transformed[2]

    def sceneInverseTransform(point):
        # point = normalizePoint(point)
        # transformed = np.matmul(fundamental_matrix.transpose(), np.array([point[0], point[1], 1.0]).transpose())
        transformed = fundamental_matrix.transpose().dot(np.array([point[0], point[1], 1.0]))
        return transformed
        # return reconstructPoint(transformed)
        # transformed = np.matmul(fundamental_matrix_opencv.transpose(), np.array([point[0], point[1], 1.0]))
        # return transformed[0], transformed[1], transformed[2]

    line1_start = []
    line1_end = []
    line2_start = []
    line2_end = []
    for point in pts1:
        a, b, c = sceneTransform(point)
        line2_start.append(np.array([0, -c / b]))
        line2_end.append(np.array([img_width, -a / b * img_width - c / b]))

    for point in pts2:
        a, b, c = sceneInverseTransform(point)
        line1_start.append(np.array([0, -c / b]))
        line1_end.append(np.array([img_width, -a / b * img_width - c / b]))

    # load images
    img_names = glob(FOLDER_NAME)
    img1 = cv2.imread(img_names[0])
    img2 = cv2.imread(img_names[1])

    # draw epipolar lines on the images
    for i in range(len(inlier_points)):
        # print(pt_start[i], pt_end[i])
        # print(transformed_pt_start[i], transformed_pt_end[i])
        img1 = cv2.line(img1, tuple(line1_start[i].astype(int)), tuple(line1_end[i].astype(int)), (255, 0, 0), 1)
        img1 = cv2.circle(img1, tuple(pts1[i].astype(int)), 3, (0, 255, 0), 1)
        img2 = cv2.line(img2, tuple(line2_start[i].astype(int)), tuple(line2_end[i].astype(int)), (255, 0, 0), 1)
        img2 = cv2.circle(img2, tuple(pts2[i].astype(int)), 3, (0, 255, 0), 1)
    # build montage to show
    np_horizontal = np.hstack((img1, img2))
    cv2.imshow('task_1_4_optional', np_horizontal)


def task_1_5_compute_essential_matrix():
    global essential_matrix

    essential_matrix = np.matmul(np.matmul(K_matrix.transpose(), fundamental_matrix), K_matrix)
    print('Essential matrix')
    print(essential_matrix)

def task_1_6_essential_matrix_decomposition():
    global essential_matrix

    essential_matrix = np.matmul(np.matmul(K_matrix.transpose(), fundamental_matrix), K_matrix)
    print('Essential matrix')
    print(essential_matrix)


if __name__ == '__main__':
    if(RUN_DOCUMENT_RESULTS):
        task_1_1_intrinsic_calibration()
        task_1_2_snapshots_for_stereo_matching()

    task_1_3_feature_matching()
    task_1_4_compute_fundamental_matrix()
    task_1_4_optional_draw_epipolar_line()
    task_1_5_compute_essential_matrix()
    task_1_6_essential_matrix_decomposition
    cv2.waitKey(0)

